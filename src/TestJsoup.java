import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class TestJsoup {
    public static void main(String[] args) {

        try {
            Connection.Response res = Jsoup.connect("https://www.rajagiritech.ac.in/stud/parent/varify.asp?action=login")
                    .data("user", "u1504045")
                    .data("pass", "15253")
                    .followRedirects(true)
                    .method(Connection.Method.POST)
                    .execute();

            Document doc = Jsoup.connect("https://www.rajagiritech.ac.in/stud/ktu/Student/Home.asp")
                    .cookies(res.cookies())
                    .get();
            System.out.println("HELLO");
            Elements rows = doc.getElementsByAttributeValueStarting("href", "Seating");
            for (Element row : rows) {
                    System.out.println(row.attr("href"));
            }

        }
        catch (IOException e){
            System.out.println(e);
        }
    }
}
